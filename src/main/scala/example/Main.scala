package example

object Main extends App {
  val ages = Seq(42, 61, 190, 64)
  println(s"The oldest person is ${ages.max}")
}
